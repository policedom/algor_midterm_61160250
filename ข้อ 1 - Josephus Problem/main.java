import java.io.*;
import java.util.Scanner;

class main {

    public static int josephus(int n, int k)  
    {
        if (n == 1) //เริ่ม
            return 0; //มีค่าเป็น 0 เมื่อมีผู้เล่นเพียงคนเดียว
        else
            return (josephus(n - 1, k) + k) % n ; //สูตรของ josephus problem
    }
 
    public static void main(String[] args)    
    {
        Scanner kb = new Scanner(System.in); 
        int n = kb.nextInt(); //n = รับค่า กำหนดจำนวนคนที่เล่นในเกม
        int k = kb.nextInt(); //k = รับค่า กำหนดจำนวนครั้งที่เลือกผู้แพ้
        System.out.println(josephus(n, k)); // josephus(n,k) = คำตอบ , index หมายเลขของผู้เล่นที่ชนะเกม
    }
}
 