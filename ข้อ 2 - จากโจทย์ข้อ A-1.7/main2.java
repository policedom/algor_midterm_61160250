import java.io.*;
import java.util.Scanner;

public class main2 {
    public static void main(String[] args) {
//        int c = 5; , int[] A = {2, 3, 8, 5};
    Scanner input = new Scanner(new InputStreamReader(System.in));
        int c = input.nextInt();
        int n = input.nextInt();
        int A[] = new int[n];
        
        for (int i=0; i<n; i++){
            A[i] = input.nextInt();
        }
        
        System.out.println(isSumEqualC(c, A));
    }
    
    public static boolean isSumEqualC(int c, int[] A) {
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (c == A[i] + A[j]) {
                    return true;
                }
            }
        }
        return false;
    }
}

